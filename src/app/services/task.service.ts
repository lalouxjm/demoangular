import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { Task } from '../models/task.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  //private _taskList: Task[] = [];

  //Observable
  private _taskList$: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>([]);

  //getter
  get taskList$(){
    return this._taskList$.asObservable();
  }

  constructor(
    //connection to api injection service
    private readonly httpClient: HttpClient
  ){
    //connect to Api and load data
    this.httpClient.get<Task[]>('http://localhost:3000/task').subscribe((tasks : Task[]) => {
      //this._taskList = tasks;
      this._taskList$.next(tasks);
    })
  }

  // get(){
  //   return this.taskList$.asObservable();
  // }

  add(task: Task): Observable<Task>{
    //connect to api and post data
    //report progress -> loader to block user actions
    return this.httpClient.post<Task>(environment.baseUri + '/task/', task, {  reportProgress: true }).pipe(
      //do another operation on the Observable after the post to be sure the database was updated
      //can't do an await because it's not a promise
      tap(task => {
        this._taskList$.value.push(task);
        //notification of observable
        this._taskList$.next(this._taskList$.value);  
      })
    );

  }

  update(task: Task): Observable<any>{
    return this.httpClient.patch(environment.baseUri + '/task/' + task.id, { isValidated: true } , { reportProgress: true })
    .pipe(
      tap(_ => {
        this._taskList$.next(this._taskList$.value);
      })
    ) 
  }

  checkTask(id: number) {
    return this.httpClient.patch<Task>(environment.baseUri + '/task/' + id, { isValidated: true }, { reportProgress: true })
    .pipe(
      tap(_ => {
        this._taskList$.next(this._taskList$.value);
      })
    )
  }

  remove(task: Task): Observable<any>{
    //connect to api and delete data
    return this.httpClient.delete('http://localhost:3000/task/' + task.id, { reportProgress: true })
      .pipe(
        tap(_ => {
          // const index = this._taskList$.value.indexOf(task);
          // this._taskList$.value.splice(index, 1);
          this._taskList$.next(this._taskList$.value.filter(t => t.id !== task.id));
        })
      );
    
  }
  
}
