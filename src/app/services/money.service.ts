import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoneyService {

  private _moneyCount: number = 0
  private _moneyCount$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  get moneyCount$(){
    return this._moneyCount$.asObservable();
  }

  constructor() { }

  add(){
    this._moneyCount++;
    this._moneyCount$.next(this._moneyCount);
  }

  remove(){
    this._moneyCount--;
    this._moneyCount$.next(this._moneyCount);
  }
  
}
