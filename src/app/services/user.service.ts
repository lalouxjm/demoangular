import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

    update(user: any){
      return this._httpClient.put( environment.baseUri + '/user/1', user, { reportProgress: true })
    }

    getById(){
      return this._httpClient.get<any>(environment.baseUri + '/user/1', { reportProgress: true })
    }

    


}
