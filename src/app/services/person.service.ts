import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Person } from '../models/person.model';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private ageApi = 'https://api.agify.io';
  private genderApi = 'https://api.genderize.io';

  // person!: Person;

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  // getAge(name: string, countryId: string): Observable<Person>{
    
  //   const url = `${this.ageApi}?name=${name}&country_id=${countryId}`;
    
  //   return this._httpClient.get<Person>(url, { reportProgress: true }).pipe(
  //     map(p => ({
  //       name: p.name,
  //       age: p.age,
  //       count: p.count,
  //       country_id: p.country_id
  //     }))
  //   );
  // }

  getAge( obj: {name: string, country_id: string }){
    const params = new HttpParams({ fromObject: obj });
    return this._httpClient.get<Person>(this.ageApi, {params, reportProgress: true });
  }

  getGender( obj: {name: string, country_id: string }){
    const params = new HttpParams({ fromObject: obj });
    return this._httpClient.get<Person>(this.genderApi, {params, reportProgress: true });
  }

  



}
