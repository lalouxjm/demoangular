import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Country } from '../models/country.model';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  getAll(): Observable<Country[]>{
    return this._httpClient.get<Country[]>('https://restcountries.com/v3.1/all', { reportProgress: true })
      .pipe(map(countries => countries.sort((c1, c2) => c1.translations.fra.common.localeCompare(c2.translations.fra.common))
      ))
  }
}
