export interface Task {
    id?: number,
    name: string;
    isValidated: boolean;
    isUrgent: boolean;
}