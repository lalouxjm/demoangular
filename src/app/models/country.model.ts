export interface Country {
  name: Name;
  tld: string[];
  cca2: string;
  ccn3: string;
  cca3: string;
  cioc: string;
  independent: boolean;
  status: string;
  unMember: boolean;
  currencies: Currencies;
  idd: Idd;
  capital: string[];
  altSpellings: string[];
  region: string;
  subregion: string;
  languages: Languages;
  translations: Translations;
  latlng: number[];
  landlocked: boolean;
  borders: string[];
  area: number;
  demonyms: Demonyms;
  flag: string;
  maps: Maps;
  population: number;
  gini: Gini;
  fifa: string;
  car: Car;
  timezones: string[];
  continents: string[];
  flags: Flags;
  coatOfArms: CoatOfArms;
  startOfWeek: string;
  capitalInfo: CapitalInfo;
  postalCode: PostalCode;
}

interface PostalCode {
  format: string;
  regex: string;
}

interface CapitalInfo {
  latlng: number[];
}

interface CoatOfArms {
  png: string;
  svg: string;
}

interface Flags {
  png: string;
  svg: string;
  alt: string;
}

interface Car {
  signs: string[];
  side: string;
}

interface Gini {
  '2018': number;
}

interface Maps {
  googleMaps: string;
  openStreetMaps: string;
}

interface Demonyms {
  eng: Eng;
  fra: Eng;
}

interface Eng {
  f: string;
  m: string;
}

interface Translations {
  ara: Deu;
  bre: Deu;
  ces: Deu;
  cym: Deu;
  deu: Deu;
  est: Deu;
  fin: Deu;
  fra: Deu;
  hrv: Deu;
  hun: Deu;
  ita: Deu;
  jpn: Deu;
  kor: Deu;
  nld: Deu;
  per: Deu;
  pol: Deu;
  por: Deu;
  rus: Deu;
  slk: Deu;
  spa: Deu;
  srp: Deu;
  swe: Deu;
  tur: Deu;
  urd: Deu;
  zho: Deu;
}

interface Languages {
  deu: string;
  fra: string;
  nld: string;
}

interface Idd {
  root: string;
  suffixes: string[];
}

interface Currencies {
  EUR: EUR;
}

interface EUR {
  name: string;
  symbol: string;
}

interface Name {
  common: string;
  official: string;
  nativeName: NativeName;
}

interface NativeName {
  deu: Deu;
  fra: Deu;
  nld: Deu;
}

interface Deu {
  official: string;
  common: string;
}