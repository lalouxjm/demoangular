export interface Person {
  age: number;
  count: number;
  country_id: string;
  name: string;
  gender: string;
  probability: number;
}