import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { TasksService } from './services/task.service';
import { Observable, map } from 'rxjs';
import { MoneyService } from './services/money.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'demoAngular';

  count$!: Observable<number>;
  money$!: number;

  constructor(
    private readonly _taskService: TasksService,
    private readonly _moneyService: MoneyService
  ){  }

  ngOnInit(): void {
    //this._taskService.taskList$.subscribe(list => this.count$ = list.length);
    this.count$ = this._taskService.taskList$.pipe(map(l => l.length));
    this._moneyService.moneyCount$.pipe(map((value: number) => {
      return Number(this.money$);
    }));
  }

  links: NbMenuItem[] = [
    { link: '/home', title: 'Home Page', icon: 'home' },
    { title: 'Demo', icon: 'star', children: [
      { link: '/demo1', title: 'Bindings, Events', icon:'bulb-outline' },
      { link: '/demo2', title: 'Directives Structurelles', icon:'bulb-outline' },
      { link: '/demo3', title: 'Injections dépendances', icon:'bulb-outline' },
      { link: '/demo4', title: 'HttpClient + Resolvers', icon:'bulb-outline' },
      { link: '/demo5', title: 'Formulaires', icon:'bulb-outline' },
    ] },
    { title: 'Exercices', icon: 'list', children: [
      { link: '/exercices/exercice1', title: 'Exercice 1', icon:'attach-outline' },
      { link: '/exercices/exercice2', title: 'Exercice 2', icon:'attach-outline' },
      { link: '/exercices/exercice3', title: 'Exercice 3', icon:'attach-outline' },
      { link: '/exercices/exercice4', title: 'Exercice 4', icon:'attach-outline' },
    ] },
    { link: '/about', title: 'About Us', icon: 'book' },
  ]
}
