import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloComponent } from './components/hello/hello.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbButtonModule, NbSidebarModule, NbMenuModule, NbCardModule, NbInputModule, NbIconModule, NbListModule, NbCheckboxModule, NbDialogModule, NbToastrModule, NbSelectModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { Demo1Component } from './pages/demo1/demo1.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Exercice1Component } from './pages/exercices/exercices/exercice1/exercice1.component';
import { Exercice2Component } from './pages/exercices/exercices/exercice2/exercice2.component';
import { ExercicesComponent } from './pages/exercices/exercices.component';
import { TimerPipe } from './pipes/timer.pipe';
import { Demo2Component } from './pages/demo2/demo2.component';
import { Exercice3Component } from './pages/exercices/exercices/exercice3/exercice3.component';
import { TaskRowComponent } from './components/task-row/task-row.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { Demo3Component } from './pages/demo3/demo3.component'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { Demo4Component } from './pages/demo4/demo4.component';
import { LoaderRadarComponent } from './components/loader-radar/loader-radar.component';
import { Exercice4Component } from './pages/exercices/exercices/exercice4/exercice4.component';
import { Demo5Component } from './pages/demo5/demo5.component';
import { FormErrorsComponent } from './components/form-errors/form-errors.component';
import { EnumPipe } from './pipes/enum.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    HelloComponent,
    Demo1Component,
    Demo2Component,
    Demo3Component,
    Demo4Component,
    Demo5Component,
    ExercicesComponent,
    Exercice1Component,
    Exercice2Component,
    Exercice3Component,
    Exercice4Component,
    TimerPipe,
    EnumPipe,
    TaskRowComponent,
    ConfirmDialogComponent,
    LoaderComponent,
    LoaderRadarComponent,
    FormErrorsComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbCardModule,
    NbInputModule,
    FormsModule,
    ReactiveFormsModule,
    NbIconModule,
    NbListModule,
    NbCheckboxModule,
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    NbSelectModule
    
  ],
  providers: [
    {provide: 'MyResource', useValue: { nom: 'Ly', methode: () => 42 }},
    //TasksService,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
''