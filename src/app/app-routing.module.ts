import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { Demo1Component } from './pages/demo1/demo1.component';
import { Exercice1Component } from './pages/exercices/exercices/exercice1/exercice1.component';
import { Exercice2Component } from './pages/exercices/exercices/exercice2/exercice2.component';
import { ExercicesComponent } from './pages/exercices/exercices.component';
import { Demo2Component } from './pages/demo2/demo2.component';
import { Exercice3Component } from './pages/exercices/exercices/exercice3/exercice3.component';
import { Demo3Component } from './pages/demo3/demo3.component';
import { Demo4Component } from './pages/demo4/demo4.component';
import { CountryResolver } from './resolvers/country.resolver';
import { Exercice4Component } from './pages/exercices/exercices/exercice4/exercice4.component';
import { Demo5Component } from './pages/demo5/demo5.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'demo1', component: Demo1Component },
  { path: 'demo2', component: Demo2Component },
  { path: 'demo3', component: Demo3Component },
  { path: 'demo4', component: Demo4Component, resolve: { countries: CountryResolver } },
  { path: 'demo5', component: Demo5Component, resolve: { countries: CountryResolver } },
  { path: 'exercices', component: ExercicesComponent, children: [
    { path: '', redirectTo: 'exercice1', pathMatch: 'full'},
    { path: 'exercice1', component: Exercice1Component },
    { path: 'exercice2', component: Exercice2Component },
    { path: 'exercice3', component: Exercice3Component },
    { path: 'exercice4', component: Exercice4Component, resolve: { countries: CountryResolver } },
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
