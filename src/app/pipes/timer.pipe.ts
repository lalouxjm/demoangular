import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timer'
})
export class TimerPipe implements PipeTransform {

  transform(value: number): string {
    

    let milliseconds = (value % 1000).toString().padStart(3, '0');
    let seconds = ('0' + Math.floor((value/1_000) % 60)).slice(-2);
    let minutes = Math.floor((value/60_000) % 60).toString().padStart(2, '0');
    let hours = Math.floor((value/3_600_000)).toString().padStart(2, '0');

    return `${hours}:${minutes}:${seconds}.${milliseconds}`;
  }

}
