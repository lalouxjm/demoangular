import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToList'
})
export class EnumPipe implements PipeTransform {

  transform(values: any): { key: any, label: string }[] {
    const enumKeys = Object.keys(values).filter(v => isNaN(Number(v)));
    return enumKeys.map(gender => ({ key: values[gender], label: gender }))
  }



}
