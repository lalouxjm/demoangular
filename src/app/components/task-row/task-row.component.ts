import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/models/task.model';
import { TasksService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-row',
  templateUrl: './task-row.component.html',
  styleUrls: ['./task-row.component.scss']
})
export class TaskRowComponent {
@Input()
task!: Task;

@Output()
onDelete: EventEmitter<Task> = new EventEmitter<Task>
// @Output()
// onUpdate: EventEmitter<Task> = new EventEmitter<Task>

constructor(
  private readonly taskService: TasksService
){}

  onClickValidate(){
    if(!this.task){
      return;
    }

    this.taskService.checkTask(<number>this.task.id)
      .subscribe(() => {
        this.task.isValidated = true;
      });
    
    //this.onUpdate.emit(this.task)
  }

  onClickDelete(){
    if(!this.task){
      return;
    }

    this.onDelete.emit(this.task)
  }
}
