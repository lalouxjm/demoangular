import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss']
})
export class HelloComponent implements OnInit, AfterViewInit {
  
  @Input() //parent vers enfants / @Output -> enfant vers parents
  name: string = 'Khun';

  @ViewChild('myItem')
  elementRef:any;

  ngOnInit(){
    console.log('le composant est initialisé! appelé avant la vue');
    //this.name = 'John';
    console.log(this.elementRef);
  }

  ngAfterViewInit(){
    console.log('appelé after ngOnInit et après la vue');
    console.log(this.elementRef);
  }

}
