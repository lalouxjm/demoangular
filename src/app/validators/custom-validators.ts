import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export class CustomValidators {
    
    static minAge(ageMin: number): ValidatorFn {
        return (control: AbstractControl): ValidationErrors|null => {
            const value: string = control.value;
            if(!value){
                return null;
            }
            //convert the string value into date
            const date: Date = new Date(value);
            if(!date){
                return { minAge: { required: ageMin } }
            }
            const today = new Date();
            let age = today.getFullYear() - date.getFullYear();
            date.setFullYear(today.getFullYear());
            if(date > today) {
                age--;
            } 
            if(age < ageMin) {
                return { minAge: {actual: age, required: ageMin} }
            }
            return null
        }
    }

    static isNiss(control: AbstractControl): ValidationErrors|null{
        const value: string = control.value;
        if(!value){
            return null;
        }

        const patern = /[0-9]{2}\.[0-9]{2}\.[0-9]{2}-[0-9]{3}\.[0-9]{2}/g;

        if(!patern.test(value)){
            return { isNiss: true }
        }

        const rawValue = value.replaceAll(/(\.|-)/g, '');

        const toControl = parseInt(rawValue.slice(0,9));
        const controlNumber = parseInt(rawValue.slice(-2));

        if(97 - (toControl % 97) === controlNumber || 97 - (toControl + 2_000_000_000 % 97) === controlNumber){
            return null;
        }
        return { isNiss: true };
    }

    static validNiss(): ValidatorFn{

        return (control: AbstractControl): { [key: string]: any } | null => {
            const value = control.value;
        
            if (!value) {
              // if the value is empty or null, don't validate
              return null;
            }
        
            // NISS must be a string of 11 digits
            if (!/^\d{11}$/.test(value)) {
              return { 'belgiumNinFormat': true };
            }
        
            // Check if the year is valid (between 00 and 99)
            const year = parseInt(value.substr(0, 2));
            const currentYear = new Date().getFullYear() % 100;
            if (year > currentYear) {
              return { 'belgiumNinYear': true };
            }
        
            // Check the checksum
            const modulo = parseInt(value.substr(0, 9)) % 97;
            const checksum = 97 - modulo;
            if (checksum !== parseInt(value.substr(9, 2))) {
              return { 'belgiumNinChecksum': true };
            }
        
            // If all checks pass, return null
            return null;
          };

    }

    static minLengthArray(min: number): ValidatorFn{
        return(control: AbstractControl): ValidationErrors|null => {
            if(control.value.length >= min){
                return null;
            }
            return { minLengthArray: true }
        }
    }
}