import { Injectable } from '@angular/core';
import {
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable} from 'rxjs';
import { Country } from '../models/country.model';
import { CountryService } from '../services/country.service';

@Injectable({
  providedIn: 'root'
})
export class CountryResolver {

  constructor(
    private readonly _countryService: CountryService
  ){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country[]> {
    return this._countryService.getAll();
  }
}
