import { Component } from '@angular/core';

@Component({
  templateUrl: './exercice2.component.html',
  styleUrls: ['./exercice2.component.scss']
})
export class Exercice2Component {
  hour: number = 0;
  minute: number = 0;
  second: number = 0;
  mili: number = 0;
  myInterval: any;
  startflag: boolean = false;
  stopflag: boolean = false;

  counter: number = 0;
  timer: any;
  previous: number = 0;

  startCorrection(){
    if(this.timer)
      return;

    const start = new Date();
    this.timer = setInterval(() => {
      this.counter = new Date().getTime() - start.getTime() + this.previous
    }, 1)
  }
  
  start(){
    this.startflag = true;
    this.stopflag = false;

    this.myInterval = setInterval(() => {
      
      this.second++;

      // if(this.mili > 999){
      //   this.mili = 0;
      //   this.second++;
      // }

      if(this.second > 59){
        this.second = 0;
        this.minute++;
      }
      if(this.minute > 59){
        this.minute = 0;
        this.hour++
      }
    }, 1000);
  }

  stop(){
    this.startflag = false;
    this.stopflag = true;

    clearInterval(this.myInterval)
    this.myInterval = null;
    //this.previous = this.counter;
  }

  reset(){
    this.stop();
    this.hour = 0;
    this.minute = 0;
    this.second = 0;
  }
}
