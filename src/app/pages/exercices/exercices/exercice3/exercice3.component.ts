import { Component, OnDestroy, OnInit } from '@angular/core';
import { Task } from '../../../../models/task.model'
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';
import { TasksService } from 'src/app/services/task.service';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './exercice3.component.html',
  styleUrls: ['./exercice3.component.scss'],
  
})
export class Exercice3Component implements OnInit, OnDestroy {

  name: string = '';
  urgent: boolean = false;
  validated: boolean = false;

  //// tasks: Task[] = [];

  //! private _sub!: Subscription;

  taskList$!: Observable<Task[]>;

  constructor(
      private readonly _dialogService: NbDialogService,
      private readonly _tasksService: TasksService,
      private readonly _toaster: NbToastrService,
      ){ }

  ngOnInit(): void {
    // this._sub = this._tasksService.taskList$.subscribe(list => this.tasks = list);
    this.taskList$ = this._tasksService.taskList$;
  }

  ngOnDestroy(): void {
    // this._sub.unsubscribe();
  }

  onClickAdd() {
    if(!this.name){
      return;
    }
    
    this._tasksService.add({ name: this.name, isUrgent: this.urgent, isValidated: false })
    .subscribe(t => {
        //if successful
        this.resetForm();
        this._toaster.success('Saved to DB ok')
      
    });

    //? this.tasks.push({ name: this.name, isUrgent: this.urgent, isValidated: false });
    //? this.tasks[...this.tasks, { name: this.name, isUrgent: this.urgent, isValidated: false }]
    //? this.name = '';
    //? this.urgent = false;
  }

  updateTask(task: Task){
    if(!task){
      return;
    }

    this._tasksService.update(task).subscribe(
      () => this._toaster.success('Task updated!')
    );
  }

  deleteTask(task: Task, message: string){
    if(!task){
      return;
    }

    //dialogBox
    const dialogRef = this._dialogService.open(ConfirmDialogComponent, {
      context: { message: 'Voulez-vous supprimer cette tâche?' }
    });
    dialogRef.onClose.subscribe(reponse => {
      if(reponse){
        // const index = this.tasks.indexOf(task);
        // this.tasks.splice(index, 1);
        //this.tasks = this.tasks.filter(t => t !== task);
        this._tasksService.remove(task).subscribe(
          () => { this._toaster.success('Task deleted!')});
      }
    });

  }

  private resetForm(){
    this.name = '';
    this.urgent = false;
  }
}
