import { Component, OnInit } from '@angular/core';
import { Observable, map } from 'rxjs';
import { MoneyService } from 'src/app/services/money.service';

@Component({
  templateUrl: './exercice1.component.html',
  styleUrls: ['./exercice1.component.scss']
})
export class Exercice1Component implements OnInit {
  countNumber: number = 0;
  moneyCount$!: Observable<Number>;

  constructor(
    private readonly _moneyService: MoneyService
  ){}

  add(){
    if(this.countNumber >= 20){
      this.alertMsg();
      return;
    }
    this.countNumber++;
    this.moneyCount$.subscribe(() => {
      this.countNumber
    })
  }

  remove(){
    if(this.countNumber<=0){
      return;
    }
    this.countNumber--;
  }

  ngOnInit(): void {
    //this.moneyCount$ = this._moneyService.moneyCount$;
    this.moneyCount$ = this._moneyService.moneyCount$.pipe(map((value: number) => {
      // Transform the emitted value into a number
      return Number(value).valueOf();
    }))
  }

  alertMsg(){
    if(this.countNumber >= 20){
      alert("You have reached the maximum amout!")
    }
  }
}
