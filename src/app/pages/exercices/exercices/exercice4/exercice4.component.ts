import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from 'src/app/models/person.model';
import { Country } from 'src/app/models/country.model';
import { PersonService } from 'src/app/services/person.service';
import { forkJoin } from 'rxjs';

@Component({
  templateUrl: './exercice4.component.html',
  styleUrls: ['./exercice4.component.scss']
})
export class Exercice4Component implements OnInit {

  firstName: string|null = null;
  selectedCountry!: Country;
  countries: Country[] = [];
  person!: Person;

  // model: { age: number, gender: string, probability: number}|null = null;


  constructor(
    private readonly _route: ActivatedRoute,
    private readonly _personService: PersonService
  ){}

  ngOnInit(): void {
    this.countries = this._route.snapshot.data['countries'];
  }

  onClick(){
    if(!this.firstName){
      return;
    }
    forkJoin([
      this._personService.getAge({name: this.firstName, country_id: this.selectedCountry.cca2}),
      this._personService.getGender({name: this.firstName, country_id: this.selectedCountry.cca2})
    ]).subscribe(([result1, result2]) => {
      this.person = { ...result1, ...result2}
    })
    

    // this._personService.getAge(this.firstName, this.selectedCountry.cca2).subscribe(
    //   person => { this.person = person; console.log(this.person.age) }
    // )

  }
}
