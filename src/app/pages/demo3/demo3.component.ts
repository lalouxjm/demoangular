import { Component, Inject } from '@angular/core';

@Component({
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component {

  constructor(
    @Inject('MyResource') private myResource: any
  ){
    console.log(myResource.nom);
    console.log(myResource.methode());
    
  }
}
