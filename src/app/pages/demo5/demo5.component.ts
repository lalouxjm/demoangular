import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { Gender } from 'src/app/enums/gender.enum';
import { Country } from 'src/app/models/country.model';
import { UserService } from 'src/app/services/user.service';
import { CustomValidators } from 'src/app/validators/custom-validators';

@Component({
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit {
  fg!: FormGroup;
  Gender = Gender;
  countries: Country[] = [];

  get skills(): FormArray {
    return <FormArray>this.fg.get('skills');
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _route: ActivatedRoute,
    private readonly _userService: UserService,
    private readonly _toaster: NbToastrService,
  ){  }

  ngOnInit(): void {
    this.countries = this._route.snapshot.data['countries']

    this.fg = this._formBuilder.group({
      lastName: [null, [Validators.required, Validators.maxLength(50)]],
      firstName: [null, [Validators.required, Validators.maxLength(50)]],
      birthDate: [null, [Validators.required, CustomValidators.minAge(18)]],
      email: [null, [Validators.required, Validators.email]],
      gender: [Gender.Undefined, [Validators.required]],
      nationality: [null, [Validators.required]],
      niss: [null, [Validators.required, CustomValidators.isNiss]],
      skills: this._formBuilder.array([], {validators: [CustomValidators.minLengthArray(1)]}),

    });

    this.fg.get('niss')?.disable();

    this.fg.get('nationality')?.valueChanges.subscribe(v => {
      if(v === 'BE'){
        this.fg.get('niss')?.enable();
      } else {
        this.fg.get('niss')?.disable();
      }
    })

    this._userService.getById().subscribe(u => {
      u.skills.forEach(() => {
        this.add();
      })
      this.fg.patchValue(u);
    })


  }

  add(){
    this.skills.markAsTouched();
    this.skills.push(this._formBuilder.group({
      name: [null, [Validators.required]],
    }))
  }

  delete(index: number){
    this.skills.markAsTouched();
    this.skills.removeAt(index);
  }

  submit(){
    if(this.fg.invalid){
      return;
    }

    this._userService.update(this.fg.value).subscribe(() => {
      this._toaster.success('Updated!');
    })

  }
}
