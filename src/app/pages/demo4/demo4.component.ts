import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Country } from 'src/app/models/country.model';

@Component({
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  countries: Country[] = [];

  constructor(
    //private readonly _countryService: CountryService
    private readonly route: ActivatedRoute
  ){}

  ngOnInit(): void {
    //this._countryService.getAll().subscribe(countries => this.countries = countries);
    this.countries = this.route.snapshot.data['countries'];
  }
}
