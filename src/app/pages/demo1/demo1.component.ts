import { Component } from '@angular/core';

@Component({
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component {
  myString: string = 'HeLlo demo !!';
  myNumber: number = 1.2456;
  myDate: Date = new Date();
  myBool: Boolean = false;

  inputValue: string = 'Jamie';

  onClick(){
    alert(this.inputValue);
  }
}
