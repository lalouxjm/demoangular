import { Component } from '@angular/core';

@Component({
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component {
  age: number = 55;

  list: string[] = ['sel', 'poivre', 'sucre'];

  jour: number = 4;
}

